from django.shortcuts import render
from todos.models import TodoList

# Create your views here.

def show_todolist(request):
    todo_lists = TodoList.objects.all()
    context = {
        'todo_lists': todo_lists
    }
    return render(request, 'lists/todo_lists.html', context)
