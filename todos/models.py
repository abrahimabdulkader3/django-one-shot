from django.db import models

# Create your models here.


# Defining the structure of our database
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now=True)


#Defining another TodoItem model
class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True) #setting null=true and blank=true allows the data to be optional
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(TodoList, on_delete=models.CASCADE, related_name="items")
